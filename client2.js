var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost', { username: 'alice', password: 'secret' });

client.on('connect', function () {
    console.log('Client started...');
    client.subscribe('presence');
});

client.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString());
});