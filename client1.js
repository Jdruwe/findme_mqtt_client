var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost', { username: 'alice', password: 'secret' });

client.on('connect', function () {
    client.subscribe('presence');
    console.log('Client publishing.. ');
    client.publish('presence', 'Client 1 is alive.. Test Ping! ' + Date());
    client.end();
});